package pl.mcmidgard.my_tablist;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;


public class My_Tablist extends JavaPlugin implements Listener {

    private short playerssize;

    @Override
    public void onEnable() {
        PluginManager pm = getServer().getPluginManager();
        pm.registerEvents(this, this);
        this.playerssize = 0;
        super.onEnable();
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerJoin(PlayerJoinEvent event) {
        playerssize++;
        updatePlayerHeaderAndFooter();
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerQuit(PlayerQuitEvent event) {
        playerssize--;
        updatePlayerHeaderAndFooter();
    }

    private void updatePlayerHeaderAndFooter() {
        String header1 = "\n§7◀ §b§lMc§3Midgard.pl §7▶\n\n§6 Gracze\n §7◀§2";
        String header2 = "§7▶\n\n§6=-=-=-[ §aONLINE §6]-=-=-=";
        String footer = "§6=-=-=-=-=-=-=-=-=-=-=\n\n§6Forum Serwera\n§2www.McMidgard.pl\n ";
        for (Player p : Bukkit.getOnlinePlayers()) {
            p.setPlayerListHeaderFooter(header1 + playerssize + header2, footer);
        }
    }

}
